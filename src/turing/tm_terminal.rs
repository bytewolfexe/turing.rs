use std::collections::HashMap;

use super::tm_transition::Transition;


pub struct Terminal {
    is_final: bool,
    transitions: HashMap<String, Transition>,
    empty_word_transition: Option<Transition>,
}

impl Terminal {
    pub fn new() -> Self {
        Self {
            is_final: false,
            transitions: HashMap::new(),
            empty_word_transition: None,
        }
    }

    pub fn is_final(&self) -> bool {
        self.is_final
    }

    pub fn transitions(&self) -> &HashMap<String, Transition> {
        &self.transitions
    }

    pub fn empty_word_transition(&self) -> &Option<Transition> {
        &self.empty_word_transition
    }

    pub fn set_final(&mut self, value: bool) {
        self.is_final = value;
    }

    pub fn add_transition(&mut self, target_terminal: &String, transition: Transition) {
        self.transitions.insert(target_terminal.to_string(), transition);
    }

    pub fn set_empty_word_transition(&mut self, transition: Transition) {
        self.empty_word_transition = Some(transition);
    }
}

impl Default for Terminal {
    fn default() -> Self {
        Self::new()
    }
}
