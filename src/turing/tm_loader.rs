use log::debug;

use super::{
    tm_util::clear_whitespace,
    tm_non_terminal::NonTerminal,
    tm_transition::Transition,
    tm_util::TapeMovement,
};


// Parses string with format "<var_name> = <var_1> <var_2> ..." into code-side types.
pub fn parse_variable_line(line: &String) -> Result<(String, Vec<NonTerminal>), String> {
    // Check validity of this line.
    if line.contains("->") {
        return Err("You can't assign variable and define state transition at the same time!".to_string());
    }
    // Into the function enters string looking like this:
    // X <- 0, 1, 2 
    let var_split:Vec<_> = line
        .split("<-")
        .map(|s| clear_whitespace(s))
        .collect();

    if var_split.len() < 2 {
        // Too few arguments.
        return Err("No <- found in variable assignment!".to_string());
    }
    else if var_split.len() > 2 {
        // Too many arguments.
        return Err("Too many <- in variable assignment!".to_string());
    }

    let var_name = var_split
        .first()
        .unwrap();

    let var_values = var_split
        .last()
        .unwrap();

    let var_values_final:Vec<_> = var_values
        .split(",")
        .map(|s| match clear_whitespace(s).as_str() {
            "000" => NonTerminal::EmptyWord,
            w => NonTerminal::Word(w.to_string()),
        })
    .collect();

    Ok((var_name.to_string(), var_values_final))
}

#[allow(unused)]
pub fn parse_rule_line(line: &String) -> Result<(String, NonTerminal, Transition), String> {
    if line.contains("<-") {
        return Err("You can't assign variable and define state transition at the same time!".to_string());
    }
    
    let rule_split:Vec<_> = line
        .split("->")
        .map(|s| clear_whitespace(s))
        .collect();

    if rule_split.len() < 2 {
        return Err("No '->' symbol found!".to_string());
    }
    else if rule_split.len() > 2 {
        return Err("There is more than one '->' operator!".to_string());
    }
    debug!("{:?}", rule_split);

    // Check validity of the expression on left hand side.
    let lhs = rule_split
        .first()
        .unwrap();

    let rhs = rule_split
        .last()
        .unwrap();

    let collect_arguments = |input: &String| -> Option<Vec<String>> {
        if let Some(input) = input.strip_prefix("(") {
            if let Some(input) = input.strip_suffix(")") {
                let arguments:Vec<_> = input
                    .split(",")
                    .map(|s| clear_whitespace(s))
                    .filter(|s| !s.is_empty())
                    .collect();
                debug!("{:?}", arguments);
                Some(arguments)
            }
            else {
                None
            }
        }
        else {
            None
        }
    };

    let (state_from, non_terminal_input) = if let Some(lhs) = collect_arguments(lhs) {
        if lhs.len() != 2 {
            // FIXME
            return Err("Too few arguments on left hand side!".to_string());
        }
        else {
            let state_from = lhs
                .first()
                .unwrap()
                .to_string();

            let non_terminal_input = match lhs.last().unwrap().as_str() {
                "000" => NonTerminal::EmptyWord,
                w => NonTerminal::Word(w.to_string()),
            };

            (state_from, non_terminal_input)
        }
    }
    else {
        return Err("Bad input on left hand side of the expression!".to_string());
    };
    
    let (state_to, replace_char, tape_movement) = if let Some(rhs) = collect_arguments(rhs) {
        if rhs.len() != 3 {
            // FIXME
            return Err("Too few arguments on right hand side!".to_string());
        }
        else {
            let state_to = rhs.get(0).unwrap().to_string();

            let replace_char = match rhs.get(1).unwrap().as_str() {
                "000" => NonTerminal::EmptyWord,
                w => NonTerminal::Word(w.to_string()),
            };

            let tape_movement = match rhs.get(2).unwrap().as_str() {
                "L" => TapeMovement::Left,
                "0" => TapeMovement::None,
                "R" => TapeMovement::Right,
                _ => {
                    return Err("Unknown tape movement operation!".to_string());
                },
            };

            (state_to, replace_char, tape_movement)
        }
    }
    else {
        return Err("Bad input on right hand side of the expression!".to_string());
    };

    let transition = Transition::new(&state_to, tape_movement, replace_char);

    Ok((state_from, non_terminal_input, transition))
}
