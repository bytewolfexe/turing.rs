#[derive(Debug)]
pub enum TapeMovement {
    Left,
    None,
    Right,
}

pub fn clear_whitespace(input: &str) -> String {
    input
        .split_whitespace()
        .filter(|s| !s.is_empty())
        .collect::<Vec<_>>()
        .join(" ")
}

#[allow(unused)]
pub fn is_whitespace(c: char) -> bool {
    c == ' ' || c == '\t' || c == '\n' 
}

