use std::collections::VecDeque;

use super::tm_non_terminal::NonTerminal;


#[derive(Debug)]
pub struct Tape {
    content: VecDeque<NonTerminal>,
    index: usize,
}

impl Tape {
    pub fn new() -> Self {
        Tape {
            content: VecDeque::new(),
            index: 0,
        }
    }

    pub fn move_left(&mut self) {
        if self.index == 0 {
            self.content.push_front(NonTerminal::EmptyWord);
        }
        else {
            self.index -= 1;
        }
    }

    pub fn move_right(&mut self) {
        if self.index == self.content.len() - 1 {
            self.content.push_back(NonTerminal::EmptyWord);
        }
        self.index += 1;
    }

    pub fn character(&self) -> &NonTerminal {
        self.content.get(self.index).unwrap()
    }

    pub fn set_character(&mut self, new_char: NonTerminal) {
        self.content[self.index] = new_char;
    }

    pub fn add(&mut self, c: char) {
        self.content.push_back(NonTerminal::Word(String::from(c)));
    }

    pub fn clear(&mut self) {
        self.index = 0;
        self.content.clear();
    }

    pub fn content(&self) -> &VecDeque<NonTerminal> {
        &self.content
    }
}
