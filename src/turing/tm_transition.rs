use crate::{NonTerminal, TapeMovement};

#[derive(Debug)]
pub struct Transition {
    next_terminal: String,
    tape_movement: TapeMovement,
    new_non_terminal: NonTerminal,
}

impl Transition {
    pub fn new(next_terminal: &String, tape_movement: TapeMovement, new_non_terminal: NonTerminal) -> Self {
        Self {
            next_terminal: next_terminal.to_string(),
            tape_movement: match tape_movement {
                TapeMovement::Left => TapeMovement::Left,
                TapeMovement::None => TapeMovement::None,
                TapeMovement::Right => TapeMovement::Right,
            },
            new_non_terminal,
        }
    }

    pub fn next_terminal(&self) -> &String {
        &self.next_terminal
    }

    pub fn tape_movement(&self) -> &TapeMovement {
        &self.tape_movement
    }

    pub fn new_non_terminal(&self) -> &NonTerminal {
        &self.new_non_terminal
    }
}

impl Clone for Transition {
    fn clone(&self) -> Self {
        Self {
            next_terminal: self.next_terminal.to_string(),
            tape_movement: match self.tape_movement {
                TapeMovement::Left => TapeMovement::Left,
                TapeMovement::None => TapeMovement::None,
                TapeMovement::Right => TapeMovement::Right,
            },
            new_non_terminal: match &self.new_non_terminal {
                NonTerminal::EmptyWord => NonTerminal::EmptyWord,
                NonTerminal::Word(w) => NonTerminal::Word(w.to_string()),
            },
        }
    }
}
