use std::collections::HashMap;

use super::{
    tm_terminal::Terminal,
    tm_util::TapeMovement,
    tm_transition::Transition,
    tm_tape::Tape,
    tm_non_terminal::NonTerminal,
};

use log::debug;


// Data structure representing Turing machine - it contains pointer to initial state,
// TODO current state, 
// list of states, and actual tape with data (by default empty).
pub struct TuringMachine {
    initial_state: Option<String>,
    terminals: HashMap<String, Terminal>,
    tape: Tape,
}


pub enum MachineState {
    Successful,
    Stuck(String, NonTerminal),
}


impl TuringMachine {
    pub fn new() -> Self {
        Self {
            initial_state: None,
            terminals: HashMap::new(),
            tape: Tape::new(),
        }
    }

    #[allow(unused)]
    pub fn add_transition(&mut self, state_name: &String, non_terminal: &NonTerminal, transition: &Transition) -> Result<(), String> {
        /*let terminal:&mut Terminal = self.terminals.entry(state_name.to_string()).or_insert(Terminal {
            is_final: false,
            transitions: HashMap::new(),
            empty_word_transition: None
        });*/
        let terminal = self
            .terminals
            .entry(state_name.to_string())
            .or_default();

        match non_terminal {
            NonTerminal::Word(w) => {
                debug!("LOAD {}", w);
                if terminal.transitions().contains_key(w) {
                    // This transition is already set! (Would result in non-deterministic
                    // automaton!)
                    //return Err("This transition is already set!".to_string());
                    return Err(format!("Transition {} is already set!", w));
                }
                else {
                    /*terminal
                        .transitions_mut()
                        .insert(w.to_string(), transition.clone());*/
                    terminal.add_transition(&w.to_string(), transition.clone());
                }
            },
            NonTerminal::EmptyWord => {
                if let None = terminal.empty_word_transition() {
                    // terminal.empty_word_transition = Some(transition.clone());
                    terminal.set_empty_word_transition(transition.clone());
                }
                else {
                    // Empty word transition is already set!
                    return Err("Empty word transition is already set!".to_string());
                }
            },
        }

        if let None = self.initial_state {
            self.initial_state = Some(state_name.to_string())
        }

        Ok(())
    }

    pub fn set_initial_state(&mut self, init_name: &str) {
        self.initial_state = Some(init_name.to_string());
    }

    pub fn make_terminal_final(&mut self, terminal: &String) -> Result<(), String> {
        /*if let Some(s) = self.terminals.get_mut(terminal) {
            s.set_final(true);
        }
        else {
            self.terminals.insert(terminal.to_string(), Terminal {
                is_final: true,
                transitions: HashMap::new(),
                empty_word_transition: None,
            });
        }*/
        let target = self.terminals.entry(terminal.to_string()).or_default();
        target.set_final(true);

        Ok(())
    }

    pub fn check_terminals_references(&self) -> Result<(), String> {
        let existing:Vec<_> = self
            .terminals
            .keys()
            .collect();

        for (_, v) in &self.terminals {
            for (_, t) in v.transitions() {
                let id = t.next_terminal();
                if !existing.contains(&id) {
                    return Err(id.to_string());
                }
            }
        }
        Ok(())
    }

    pub fn load_tape(&mut self, tape: &str) {
        self.tape.clear();
        tape
            .chars()
            .for_each(|c| self.tape.add(c));
    }


    pub fn run(&mut self) -> Result<MachineState, String> {

        let mut current_terminal_id = match &self.initial_state {
            Some(t) => t,
            None => {
                return Err("Could not find initial state!".to_string());
            },
        };

        'running: loop {
            debug!("{:?}", self.tape);

            let current_terminal = match self.terminals.get(current_terminal_id) {
                Some(t) => t,
                None => {
                    return Err("Bad reference!".to_string());
                }
            };

            let current_symbol = self.tape.character();

            debug!("Character loaded from tape : {:?}", current_symbol);

            let transition = match current_symbol {
                NonTerminal::Word(w) => {
                    if current_terminal.transitions().contains_key(w) {
                        current_terminal.transitions().get(w).unwrap()
                    }
                    else if let Some(nt) = &current_terminal.empty_word_transition() {
                        nt
                    }
                    else {
                        debug!("Ended with word : {}", w);
                        break 'running;
                    }
                },
                NonTerminal::EmptyWord => {
                    if let Some(t) = &current_terminal.empty_word_transition() {
                        t
                    }
                    else {
                        debug!("Ended without word");
                        break 'running;
                    }
                }
            };
            debug!("Transition determined : {:?}", transition);

            current_terminal_id = transition.next_terminal();

            match transition.new_non_terminal() {
                NonTerminal::Word(w) => {
                    self.tape.set_character(NonTerminal::Word(w.to_string()));
                },
                NonTerminal::EmptyWord => {
                    self.tape.set_character(NonTerminal::EmptyWord);
                }
            }
            match transition.tape_movement() {
                TapeMovement::Left => {
                    self.tape.move_left();
                },
                TapeMovement::Right => {
                    self.tape.move_right();
                },
                _ => {},
            }
            debug!("State of machine after run : {:?}\n{:?}", self.tape, current_terminal_id);
        }

        if self.terminals.get(current_terminal_id).unwrap().is_final() {
            return Ok(MachineState::Successful);
        }
        else {
            return Ok(MachineState::Stuck(current_terminal_id.to_string(), self.tape.character().clone()));
        }
    }

    pub fn initial_state(&self) -> &Option<String> {
        &self.initial_state
    }

    pub fn terminals(&self) -> &HashMap<String, Terminal> {
        &self.terminals
    }

    pub fn tape(&self) -> &Tape {
        &self.tape
    }
}
