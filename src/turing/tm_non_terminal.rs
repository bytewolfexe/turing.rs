

#[derive(Debug)]
pub enum NonTerminal {
    EmptyWord,
    Word(String)
}

impl Clone for NonTerminal {
    fn clone(&self) -> Self {
        match self {
            NonTerminal::Word(w) => {
                NonTerminal::Word(w.to_string())
            },
            NonTerminal::EmptyWord => {
                NonTerminal::EmptyWord
            },
        }
    }
}

impl PartialEq for NonTerminal {
    fn eq(&self, other: &Self) -> bool {
        if let NonTerminal::Word(w) = self {
            if let NonTerminal::Word(w2) = other {
                if w == w2 {
                    true
                }
                else {
                    false
                }
            }
            else {
                false
            }
        }
        else {
            if let NonTerminal::EmptyWord = other {
                true
            }
            else {
                false
            }
        }
    }
}


impl Eq for NonTerminal {}
