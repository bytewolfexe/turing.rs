mod turing;


use std::{
    collections::HashMap,
    fs::File,
    io::{
        BufReader,
        BufRead,
        Write
    },
};

use log::{
    error,
    debug,
    log_enabled
};

use turing::{
    tm_loader::parse_rule_line,
    tm_loader::parse_variable_line,
    tm_machine::MachineState,
    tm_machine::TuringMachine,
    tm_non_terminal::NonTerminal,
    tm_transition::Transition,
    tm_util::clear_whitespace,
    tm_util::TapeMovement,
};


fn main() {
    env_logger::init();

    let mut cmd_args:Vec<_> = std::env::args().collect();

    cmd_args.remove(0);


    if cmd_args.is_empty() {
        //eprintln!("No filename was provided!");
        error!("No filename was provided!");
        return;
    }

    let filename = cmd_args.last().unwrap();

    let file = match File::open(filename) {
        Ok(f) => f,
        Err(_) => {
            //eprintln!("Could not open file {}!", filename);
            error!("Could not open file {}!", filename);
            return;
        }
    };

    let reader = BufReader::new(file);

    let lines = reader.lines().flatten();

    let mut machine = TuringMachine::new();

    let mut final_terminals:Vec<String> = vec![];

    let mut line_counter = 0;

    let mut variables:HashMap<String, Vec<NonTerminal>> = HashMap::new();

    for line in lines {
        if line.is_empty() {
            // Do nothing
        }
	else if line.starts_with("#") {
	    // This line is a comment - we can ignore it!
	}
        else if let Some(args) = line.strip_prefix(":ACCEPT ") {
            args
                .split(",")
                .map(|s| clear_whitespace(s))
                .filter(|s| !s.is_empty())
                .for_each(|s| final_terminals.push(s.to_string()));
        }
        else if let Some(args) = line.strip_prefix(":START ") {
            // TODO check if this is actually valid name (does not contain ';', etc.)
            // TODO OR MAYBE even better, make it store the results and then iterate them 
            // all and output every single result (multi-execution)!!!
            machine.set_initial_state(&clear_whitespace(args));
        }
        else if line.contains("<-") {
            debug!("Variable detected!");
            let variable_result = parse_variable_line(&line);
            debug!("{:?}", variable_result);

            match variable_result {
                Ok((name, values)) => {
                    variables.insert(name, values);
                },
                Err(e) => {
                    error!("{}", e);
                    return;
                }
            }
        }
        else if line.contains("->") {
            debug!("Transition detected!");
            // TODO check if variable :3
            // Matching as state transition definition.
            let (state_from, non_terminal_input, transition) = match parse_rule_line(&line) {
                Ok(collection) => collection,
                Err(e) => {
                    error!("Error at line {} : {}", line_counter, e);
                    return;
                },
            };

            // TODO add EmptyWord branch.
            if let NonTerminal::Word(w) = &non_terminal_input {
                // Check if loaded word is not a variable name.
                if variables.contains_key(&w.to_string()) {
                    let variable = variables
                        .get(&w.to_string())
                        .unwrap();  // Already checked, so this is safe! :)

                    for value in variable {
                        debug!("Adding variable {:?}", value);
                        // Cloning variable tape_movement from borrow.
                        let tape_movement = match transition.tape_movement() {
                            TapeMovement::Left => TapeMovement::Left,
                            TapeMovement::None => TapeMovement::None,
                            TapeMovement::Right => TapeMovement::Right,
                        };
                        // The new transition.
                        // FIXME make this actually work please :3
                        let transition = if transition.new_non_terminal() == &NonTerminal::Word(w.to_string()) {
                            debug!("This is the variable!");
                            Transition::new(
                                transition.next_terminal(),
                                tape_movement,
                                value.clone()
                            )
                        }
                        else {
                            debug!("This is not the variable!");
                            Transition::new(
                                transition.next_terminal(),
                                tape_movement,
                                transition.new_non_terminal().clone()
                            )
                        };

                        debug!("{:?}", transition);
                        // The actual insertion operation.
                        match machine.add_transition(&state_from, &value, &transition) {
                            Ok(()) => (),
                            Err(e) => {
                                error!("{}", e);
                                return;
                            },
                        }
                    }
                }
                else {
                    match machine.add_transition(&state_from, &non_terminal_input, &transition) {
                        Ok(()) => {
                            debug!("Successfully loaded transition from file!");
                        },
                        Err(e) => {
                            error!("Error at line {} : {}", line_counter, e);
                            return;
                        }
                    }
                }

            }
            else {
                match machine.add_transition(&state_from, &NonTerminal::EmptyWord, &transition) {
                    Ok(()) => (),
                    Err(e) => {
                        error!("Error at line {} : {}", line_counter, e);
                        return;
                    },
                }
            }
        }
        else {
            error!("Invalid line - could not determine what to do with this line!");
            return;
        }

        line_counter += 1;
    }

    for final_terminal in final_terminals {
        match machine.make_terminal_final(&final_terminal) {
            Ok(()) => (),
            Err(e) => {
                //eprintln!("FATAL : {}", e);
                error!("FATAL : {}", e);
                return;
            },
        }
    }

    if log_enabled!(log::Level::Debug) {
        //println!("{:?}", machine.initial_state());
        debug!("{:?}", machine.initial_state());
        for (k, v) in machine.terminals() {
            //println!("{} : ", k);
            debug!("{} : ", k);
            for (m, n) in v.transitions() {
                //println!("{} : {:?}", m, n);
                debug!("\t{} : {:?}", m, n);
            }
        }
    }


    // TODO maybe change this function so it does not 
    match machine.check_terminals_references() {
        Ok(()) => (),
        Err(e) => {
            error!("Undefined terminals found - {} is not defined!", e);
            return;
        }
    }

    println!("=== Enter word to check with given machine ===");

    'running: loop {
        print!(">> ");
        std::io::stdout()
            .flush()
            .unwrap();

        let mut input_state = String::new();
        match std::io::stdin().read_line(&mut input_state) {
            Ok(_) => {
            },
            Err(_) => {
                //eprintln!("I/O Error!");
                error!("I/O Error!");
                return;
            },
        }
        input_state = input_state.chars().filter(|&c| c != '\n').collect();

        println!("{:?}", input_state);
        if input_state.is_empty() {
            break 'running;
        }

        else {
            machine.load_tape(&input_state);
            match machine.run() {
                Ok(m) => {
                    match m {
                        MachineState::Successful => {
                            println!("Accepted!");
                        },
                        MachineState::Stuck(s, c) => {
                            match c {
                                NonTerminal::Word(w) => {
                                    println!("Stuck in state {} : character {}!", s, w);
                                },
                                NonTerminal::EmptyWord => {
                                    println!("Stuck in state {} : character EMPTY WORD!", s);
                                },
                            }
                        }
                    }
                },
                Err(e) => {
                    //eprintln!("{}", e);
                    error!("{}", e);
                }
            }
            println!("State of tape after the operation : ");
            machine.tape().content().iter().for_each(|c| {
                match c {
                    NonTerminal::Word(w) => {
                        print!("| {} ", w);
                    },
                    NonTerminal::EmptyWord => {
                        print!("| φ ");
                    },
                }
                std::io::stdout()
                    .flush()
                    .unwrap();
            });
            println!("|");

            print!("Plain text : ");
            machine
                .tape()
                .content()
                .iter()
                .for_each(|c| {
                    match c {
                        NonTerminal::EmptyWord => print!("φ"),
                        NonTerminal::Word(w) => print!("{}", w),
                    }
                });
            for _ in 0..3 {
                println!();
            }
        }
    }
}
