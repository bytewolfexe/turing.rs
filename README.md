# turing.rs - Deterministic Turing machine implementation in Rust

## Table of contents

- [Overview](#overview)

- [Usage](#usage)

    - [Pre-required software](#pre-required-software)

    - [Steps to build the program](#steps-to-build-the-program)

    - [Steps to run the program](#steps-to-run-the-program)

- [Roadmap](#roadmap)

- [Licensing](#licensing)


## Overview

Turing machine is a machine, which works on simple principle - you have tape on which
you hold some data you want to process (maybe even more tapes, for example one for 
input other for output), you have control unit which actually performs some operations
on provided data, outputs new data and moves the tape(s) accordingly.

Despite its simplicity, Turing machine is still capable of implementing almost any algorithm - that is, if you are creative enough ;) 

`turing.rs` aims to create a simple yet effective and checked environment for Turing
machine simulation.


## Usage

To run `turing.rs`, you have to first clone and build this project.


### Pre-required software

- working rust environment with access to `cargo` tool

- git


### Steps to build the program

1. use `git clone https://codeberg.org/bytewolfexe/turing.rs` to clone this project repository

2. navigate into the cloned project's directory

3. use `cargo build --release` command to build the project in release mode.

4. (optional) copy the compiled executable from `target/release` directory into your desired directory.


### Steps to run the program

1. write your machine rules into plain-text file (see [Input file format](#input-file-format) for more information)

2. start the program by using `cargo run` or by calling the compiled executable, and pass the input file path as 
last argument (for example `cargo run file.txt` or `./turing file.txt` (UNIX alternative)). You can also find
some example files in `examples` folder!

3. if your rule file does not contain any issues, interactive mode will open (indicated by `>>` sign).

4. You can now pass in words you want to test with your machine!

5. After you finish, you can safely terminate the program by just pressing Enter, without entering anything.


### Input file format

`turing.rs` is using a simple format that defines individual states and transition between states.
Currently, `turing.rs` can only solve deterministic Turing machines, so if you try to read same input
from single state, program will return an error message.

The input format looks like this:

```
:START q0
:ACCEPT qf

q0 -> a; q1; a'; R
...
```

Now, let's look at each line and its importance:

- First line, `:START q0` says that we will be starting in state named `q0`

- Second line, `:ACCEPT qf`, defines states that will be accepted as finish lines for 
the machine.

- Following would be a list of individual transition rules. Format of transition
rules follows this pattern: $S \rightarrow n; S_{next}; n_{write}; move$, where `S` would 
represent state which we are making transition from, $n$ represents 
terminal character that needs to be loaded, $S_{next}$ represents state we will be transfering
to, $n_{write}$ represents character that will be written back onto the tape, and 
$move$ represents direction the tape will move (L = left, R = right, 0 = no movement).

- Note that actual order you specify the rules in does not matter - `turing.rs` will check
validity of your rules before beginning execution!


## API reference

TODO `¯\_(ツ)_/¯`


## Roadmap

- ✅ Implemented conceptual TUI application

- ❌ Polishing of the API so it is more usable in other applications.

- ❌ Creating desktop-based GUI program running on `turing.rs` backend

- ❌ Creating web-based GUI program running on `turing.rs` backend


## Licensing 

This program was written by Tomas Caplak and is distributed under [GNU GPL ver. 3](https://www.gnu.org/licenses/gpl-3.0.en.html#license-text) license.

<div style="justify-content:center">![](./image.png)</div>
